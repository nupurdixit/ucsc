import java.sql.SQLException;
import java.util.Scanner;

public class CustomerApp {
	static Scanner sc=new Scanner(System.in);
	static CustomerDB cdb=new CustomerDB();
	public static void displayCommandMenu(){
		String displayMessage="COMMAND MENU\n"+"list     -  List all customers\n"+
	                                           "add      -  Add a customer\n"+
				                               "del      -  Delete a customer\n"+
	                                           "update   -  Update a customer\n"+
	                                           "help     -  Show this menu\n"+
				                               "exit     -  Exit this application\n";
		System.out.println(displayMessage+"\n");
		
	}
	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub
		//CustomerDB c=new CustomerDB();
		
		System.out.println("Welcome to the Customer Maintenance Application\n");
		displayCommandMenu();
		String commandChoice="";
		while(!commandChoice.equalsIgnoreCase("exit")){
			System.out.println();
			commandChoice=Validator.getString(sc, "Enter a command:");
			
			switch(commandChoice){
				case "list":
					System.out.println("CUSTOMER LIST");
					cdb.getCustomers();
					break;
				case "add":
					addCustomer();
					
					break;
				case "update":
					updateCustomer();
					break;
				case "del":
					deleteCustomer();
					break;
				case "help":
					displayCommandMenu();
					break;
				case "exit":
					exit();
					break;
				default:
					System.out.println("Invalid choice");
					break;
			}
			
		}
			
			
}
		
	

	public static void addCustomer() throws SQLException{
		String emailAddress=Validator.getString(sc, "Enter Customer Email Address");
		String firstName=Validator.getString(sc, "Enter first name");
		String lastName=Validator.getString(sc, "Enter last name");
		Customer c=new Customer(firstName,lastName,emailAddress);
		//CustomerDB cdb=new CustomerDB();
		c.setEmailAddress(emailAddress);
		c.setFirstName(firstName);
		c.setLastName(lastName);
		cdb.addCustomer(c);
		System.out.println();
		System.out.println(firstName + " "+lastName+" was added to the database");
	}
	
	public static void deleteCustomer() throws SQLException{
		String emailAddress=Validator.getString(sc, "Enter Customer Email Address");
		Customer c=cdb.getCustomer(emailAddress);
		cdb.deleteCustomer(c);
		System.out.println();
		System.out.println(c.getFirstName() + " "+c.getLastName()+" was deleted from the database");
	}
	
	public static void updateCustomer() throws SQLException{
		String emailAddress=Validator.getString(sc, "Enter Customer Email Address");
		Customer c=cdb.getCustomer(emailAddress);
		if(c!=null){
			String firstName=Validator.getString(sc, "Enter first name\n");
			String lastName=Validator.getString(sc, "Enter Last Name\n");
			c.setFirstName(firstName);
			c.setLastName(lastName);
			cdb.updateCustomer(c);
			System.out.println();
			System.out.println(c.getFirstName() + " "+c.getLastName()+" has been updated in the database");	
		}
		else
			System.out.println("No row existing for this Emailaddress in the database");
			
	}
	
	public static void exit(){
		System.out.println("Bye");
		System.out.println();
		//System.exit(0);
	}

}
