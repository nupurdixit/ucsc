//package demoday7;


import java.text.NumberFormat;

public class Customer
{
    private String firstName;
    private String lastName;
    private String emailAddress;

    public Customer()
    {
        this("", "", "");
    }

    public Customer(String firstName, String lastName, String emailAddress)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
    }

    public void setFirstName(String firstName)
    {
    	this.firstName = firstName;
    }

    public String getFirstName(){
        return firstName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setEmailAddress(String emailAddress)
    {
        this.emailAddress = emailAddress;
    }

    public String getEmailAddress()
    {
        return emailAddress;
    }

//    public String getFormattedPrice()
//    {
//        NumberFormat currency = NumberFormat.getCurrencyInstance();
//        return currency.format(price);
//    }
//
//    public boolean equals(Object object)
//    {
//        if (object instanceof Customer)
//        {
//            Customer product2 = (Customer) object;
//            if
//            (
//                code.equals(product2.getCode()) &&
//                description.equals(product2.getDescription()) &&
//                price == product2.getPrice()
//            )
//                return true;
//        }
//        return false;
//    }

    public String toString()
    {
        return "First Name:        " + firstName + "\n" +
               "Last Name: " + lastName + "\n" +
               "Email Address:       " + emailAddress + "\n";
    }
}