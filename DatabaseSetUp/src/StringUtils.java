//Utility to pad spaces in a string for formatting purpose

public class StringUtils {
	
	public static String padWithSpaces(String str,int length){
		
		StringBuilder sb=new StringBuilder(str);
		
		while(str.length() < length){
			sb.append(" ");
			length--;
		}
		return sb.toString();
		
	}
}
