import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CustomerInvoiceApp {
	private Connection connect(){
		Connection connection=null;
		try{
			String dbDirectory="Resources";
			System.setProperty("derby.system.home", dbDirectory);
			String url="jdbc:derby:MurachDB";
			String username="";
			String password="";
			connection=DriverManager.getConnection(url,username,password);
		}
		catch(SQLException e){
			System.err.println("Error loading database: "+e);
		}
		return connection;
		
	}
	
	public void getData() throws SQLException{
		Connection connection=connect();
		
		//In order to get $ before price
		NumberFormat currency=NumberFormat.getCurrencyInstance();
		
		//this is to change the date format from dd-mm-yyyy to mm/dd/yy..
		//please mention 'd' instead of 'D' as d denotes day and D denotes number of days.
		DateFormat df=new SimpleDateFormat("MM/dd/YY");
		
		try{
//			System.out.println("SELECT c.EmailAddress,i.InvoiceNumber,i.InvoiceDate,i.InvoiceTotal "+
//					"FROM Customers c,Invoices i "+
//					"WHERE c.CustomerId=i.CustomerId ORDER BY c.EmailAddress");
			String query="SELECT c.EmailAddress,i.InvoiceNumber,i.InvoiceDate,i.InvoiceTotal "+
					"FROM Customers c,Invoices i "+
					"WHERE c.CustomerId=i.CustomerId ORDER BY c.EmailAddress";
			PreparedStatement ps=connection.prepareStatement(query);
			ResultSet rs=ps.executeQuery();
			
			System.out.println("Welcome to the Customer Invoices Report\n");
			
			while(rs.next()){
				
				String email=rs.getString("EmailAddress");
				String invoiceNumber=rs.getString("InvoiceNumber");
				Date InvoiceDate=rs.getDate("InvoiceDate");
				Double InvoiceTotal=rs.getDouble("InvoiceTotal");
				System.out.print(StringUtils.padWithSpaces(email, 30)+StringUtils.padWithSpaces(invoiceNumber, 6)+"   "+df.format(InvoiceDate)+"    "+currency.format(InvoiceTotal)+"\n");
			}
			
			connection.close();
			ps.close();
			rs.close();
			
		}catch(SQLException e){
			System.out.println("Error in fetching rows"+e);
			connection.close();
		}
			
	}
	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub
		CustomerInvoiceApp ci=new CustomerInvoiceApp();
		ci.getData();
		
	}

}
