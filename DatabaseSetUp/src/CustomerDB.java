import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

//import demoday7.Product;

public class CustomerDB {
	private int ID=20;
	private Connection connect(){
		Connection connection=null;
		try{
			String dbDirectory="Resources";
			System.setProperty("derby.system.home", dbDirectory);
			String url="jdbc:derby:MurachDB";
			String username="";
			String password="";
			connection=DriverManager.getConnection(url,username,password);
		}
		catch(SQLException e){
			System.err.println("Error loading database: "+e);
		}
		return connection;
		
	}
	
	public ArrayList<Customer> getCustomers() throws SQLException{
		Connection connection=connect();
		try{
			ArrayList<Customer> customers=new ArrayList<>();
			
			String query="SELECT * "+"FROM Customers";
			PreparedStatement ps=connection.prepareStatement(query);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				String firstName=rs.getString("FirstName");
				String lastName=rs.getString("LastName");
				String emailAddress = rs.getString("EmailAddress");
				Customer c=new Customer(firstName,lastName,emailAddress);
				customers.add(c);
			}
			for(Customer c:customers){
				System.out.println(StringUtils.padWithSpaces(c.getEmailAddress(),30)+StringUtils.padWithSpaces(c.getFirstName(), 15)+StringUtils.padWithSpaces(c.getLastName(),15));
			}
			rs.close();
			ps.close();
			connection.close();
			return customers;
		}
		catch(SQLException e ){
			connection.close();
			System.err.println("Error in retrieving the data from produts table :"+e);
			return null;
		}
		
	
	}
	
	
	
	public boolean updateCustomer(Customer c) throws SQLException
    {
		 Connection connection = connect();
        try
        {
           
            String update =
                "UPDATE Customers SET " +
                    "FirstName = ?, " +
                    "LastName = ? " +
                "WHERE EmailAddress = ?";
            PreparedStatement ps = connection.prepareStatement(update);

            ps.setString(1, c.getFirstName());
            ps.setString(2, c.getLastName());
            ps.setString(3,c.getEmailAddress());
            ps.executeUpdate();
            ps.close();
            connection.close();
            return true;
        }
        catch(SQLException sqle)
        {	
        	
        	connection.close();
            //sqle.printStackTrace();   // for debugging
            return false;
        }
    }
	
	public Customer getCustomer(String emailAddress) throws SQLException{
		Connection connection=connect();
		try{
			
			
			String query="SELECT * "+"FROM Customers "+
			             "WHERE EmailAddress=? ";
			PreparedStatement ps=connection.prepareStatement(query);
			ps.setString(1, emailAddress);
			ResultSet rs=ps.executeQuery();
			if(rs.next()){
				
				String firstName=rs.getString("FirstName");
				String lastName=rs.getString("LastName");
				//String emailAddress = rs.getString("EmailAddress");
				Customer c=new Customer(firstName,lastName,emailAddress);
				rs.close();
				ps.close();
				connection.close();
				return c;
				
			}
			else {
				//System.out.println("No row existing for this Emailaddress in the database");
				connection.close();
				return null;
			}
			
			
		}
		catch(SQLException e ){
			connection.close();
			System.err.println("Error in retrieving the data from produts table :"+e);
			return null;
		}
	}
	
	public boolean addCustomer(Customer c) throws SQLException{
		Connection connection=connect();
		try{
			
			String query="Insert into Customers VALUES(?,?,?,?)";
			PreparedStatement ps=connection.prepareStatement(query);
			ps.setInt(1,ID++);
			ps.setString(2,c.getFirstName());
			ps.setString(3, c.getLastName());
			ps.setString(4, c.getEmailAddress());
			ps.executeUpdate();
			ps.close();
			connection.close();
			return true;
		}
		
		catch(SQLException e){
			connection.close();
			return false;
		}
	}
	
	public boolean deleteCustomer(Customer c) throws SQLException{
		Connection connection=connect();
		try{
			
			String query="Delete from Customers WHERE emailAddress=?";
			PreparedStatement ps=connection.prepareStatement(query);
			ps.setString(1,c.getEmailAddress());
			ps.executeUpdate();
			ps.close();
			connection.close();
			return true;
		}
		
		catch(SQLException e){
			connection.close();
			return false;
		}
	}
//	public static void main(String[] args) {
//		// TODO Auto-generated method stub
//		CustomerDB tm=new CustomerDB();
//		
//		//tm.getRows();
//		//tm.updateProduct();
//		tm.getCustomers();
//		
//	}

}
