package OrderQueue_Solution_Better;
public class OrderHandler extends Thread
{
	private static Object obj = new Object();
    private OrderQueue orderQueue;

    public OrderHandler(OrderQueue orderQueue)
    {
        this.orderQueue = orderQueue;
    }

    @Override
    public void run()
    {
        Order order;
        while (true)
        {
        	synchronized (obj) {
            order = orderQueue.pullOrder();    // get next available order           
            System.out.println(
                "                              " + order.toString() + 
                " processed by " + this.getName());
        	}
        	try
            {
                Thread.sleep(2000);            // delay two seconds
            }
            catch (InterruptedException e) {}  // ignore interruptions
        }
    }
}