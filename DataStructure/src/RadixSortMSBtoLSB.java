
public class RadixSortMSBtoLSB {
	public static void sort(int[] a){
		int nb=numberOfDigits(a);
		rs(nb,a);
	}
	
	public static int numberOfDigits(int[] a){
		
		int max=a[0];
		for(int i=1;i<a.length;i++){
			if(a[i]>max){
				max=a[i];
			}
			
		}
		System.out.println("Max element is: "+max);
		//int nb=max;
		int b=0;
		while(max!=0){
			max=max/2;
			b++;
			
		}
		System.out.println("number of bits are: "+b);
		return b;
		
	}
	
	public static void rs(int nb,int[] a){
		for(int i=nb;i>0;i--){
			rs1(nb,a);
		}
	}
	
	public static boolean assertAscending(int[] a){
		boolean flag=true;
		if(a.length>0){
			flag=assertAscending(a,0,a.length-1);
		}
		return flag;
	}
	
	public static boolean assertAscending(int[]a,int start, int end){
		int min=a[0];
		for(int i=start+1;i<=end;i++){
			if(a[i]<min){
				return false;
			}
			min=a[i];
		}
		return true;
	}
	
	public static int[] rs1(int nb,int[] a){
		int mask=1>>nb;
			System.out.println("mask is: "+mask);
		int[] zeroa=new int[a.length];
		int[] onea=new int[a.length];
		int t0=0;
		int t1=0;
		for(int i=0;i<a.length;i++){
			if((a[i] & mask)!=0){
				onea[t1++]=a[i];
			}
			else{
				zeroa[t0++]=a[i];
			}
		}
//		if(!assertAscending(onea)){
//			rs1(nb,onea);
//		}
//		if(!assertAscending(zeroa)){
//			rs1(nb,zeroa);
//		}
		
		int k=0;
		for(int i=0;i<t0;i++){
			a[k++]=zeroa[i];
		}
		for(int i=0;i<t1;i++){
			a[k++]=onea[i];
		}
		
		
		return a;
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] a={3,0,1,2};
		sort(a);
		System.out.print("Sorted array is: ");
		for(int num:a){
			System.out.print(num+" ");
		}
	}

}
