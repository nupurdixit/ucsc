import java.util.ArrayList;

/**
 * Program to find the maximum difference between any two elements in an array with the second numbers index greater than the first
 * and value also greater than the first.
 * 
 * @author NUPUR
 *
 */
public class MaxDifference {
	
	public static int findMaxDifference(int[] a){
		int diff=-1;
		int maxDiff=0;
		for(int i=0;i<a.length;i++){
			for(int j=i;j<a.length;j++){
				//if(j>i & a[j]>a[i]){
					diff=a[j]-a[i];
					if(diff > maxDiff)
						maxDiff=diff;
//				}
//				else{
//					continue;
//				}
			}
		}
		
		System.out.println("max difference is: "+maxDiff);
		return maxDiff;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] a={14,21,20,8,12,15};
		findMaxDifference(a);
	}

}
