import java.util.Comparator;
import java.util.HashMap;
import java.util.PriorityQueue;

public class TopFour implements Comparator<Integer>{
	/*
	 * method to return the top four elements in the array
	 */
	public static void returnTopFour(int[] a){
		TopFour pqs=new TopFour();
		
		//Pass the two objects to the PQ so that they can be compared in the compare class
		PriorityQueue<Integer> pq=new PriorityQueue<Integer>(1,pqs);
		for(int i:a){
			pq.add(i);
		}

		//print the top four elements
		int j=1;
		while(j<5){
			System.out.println(pq.poll());
			j++;
		}
		
	}

	@Override
	public int compare(Integer i1,Integer i2) {
		
		//to return the max element first
		return i2-i1;
		
		//to return the smallest first
		//return i1-i2;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] a={5,10,1,2,3,8,66,66,88,23,9};
		returnTopFour(a);
	
	}
	
}


