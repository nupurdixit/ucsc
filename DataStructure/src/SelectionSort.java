/*
 * SELECTION SORT: O(N): N is the number of elements in the array
 */
public class SelectionSort {
	
	/*
	 * Method to sort the array via recursion
	 */
	public static int[] sortedArray(int[] a){
		int i=0;
		int j=a.length-1;
		while(i<=j){
			int x=findMin(a,i,j); //find the minimum element in the array recursively
			swap(a,i,x); //swap each position starting from first with the minimum element
			i++;
			
		}
		
		//Print the sorted array
		System.out.print("sorted array is: ");
		for(int num:a){
			System.out.print(num+" ");
		}
		return a;
	}
	
	/*
	 * Method to find the minimum element
	 */
	public static int findMin(int[]a,int i,int j){
		int p=i;
		int v=a[p];
		for(int k=i+1;k<=j;k++){
			if(a[k]<v){
				p=k;
				v=a[k];
			}
			
		}
		return p;
	}
	
	
	/*
	 * Method to swap two elements in an array
	 */
	public static void swap(int[]a,int x,int y){
		int t=a[x];
		a[x]=a[y];
		a[y]=t;
	}
	
	/*
	 * Main function which calls the method to sort the given array
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] a={4,2,1,9,8,6,4};
		sortedArray(a);
	}

}
