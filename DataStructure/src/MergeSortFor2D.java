/*
 * MERGE SORT ON 2D ARRAY
 */
public class MergeSortFor2D {


	public static void mergeSort(int start,int end,int[] t,int[] a){
		int mid=(end-start)/2+start;
		if(end-start>1){
			mergeSort(start,mid,t,a);
			mergeSort(mid,end,t,a);
			merge(start,mid,end,t,a);
		}
		
	}
		
	public static void merge(int start,int mid,int end,int[] t,int[] a){
		int i=start;
		int j=mid;
		int k=0;
		while(i<mid && j<end){
			if(a[i]<a[j]){
				t[k++]=a[i++];
			}
			else {
				t[k++]=a[j++];
			}
		}
		while(i<mid){
			t[k++]=a[i++];
		}
		while(j<end){
			t[k++]=a[j++];
		}
		
		for(int f=0;f<k;f++){
			int p=start+f;
			int v=t[f];
			a[p]=v;
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int[][] b={{4,3,6,71,16},{22,12,45,34,78},{342,121,120,234,678},{2,56,34,22,12}};
		int[] a=new int[b.length*b[0].length];
		int k=0;
		for(int i=0;i<b.length;i++){
			for(int j=0;j<b[i].length;j++){
				a[k++]=b[i][j];
			}
		}
		int[] t=new int[a.length];
		mergeSort(0,a.length,t,a);
		
		System.out.println("Sorted array is: ");
		for(int num:a){
			System.out.println(num+" ");
		}
	}
	

}

