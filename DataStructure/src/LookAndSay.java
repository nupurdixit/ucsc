/*
 * This program genrates sequence based on the occurence of digit e.g. 1 -> 11 since 1 occured 1 time.
 * 11-> 21 -> 1211 (since 2 occured 1 time and 1 occured 1 time , thus 1211) and so on..
 */
public class LookAndSay {
	public static String lookAndSay(String number){
		StringBuilder sb=new StringBuilder();
		int position=0;
		String digitsTraversed;
		
		for(int i=0;i<number.length();i++){
			if(number.charAt(position)!=number.charAt(i)){
				digitsTraversed=number.substring(position, i);
				sb.append(digitsTraversed.length()).append(number.charAt(position));
				position=i;
			}
		}
		sb.append(number.substring(position,number.length()).length()).append(number.charAt(position));
		System.out.println("String is: "+sb.toString());
		
		return sb.toString();
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		lookAndSay("112211");
		
	}

}
