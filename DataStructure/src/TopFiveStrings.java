import java.util.HashMap;
import java.util.PriorityQueue;

public class TopFiveStrings {
	public static void returnTopFive(String[] a){
		
		HashMap<String,Integer> map=new HashMap<>();
		for(String s:a){
			map.put(s, map.get(s)==null?1:map.get(s)+1);
		}
		
		
		PriorityQueue<Node> pq=new PriorityQueue<Node>();
		
		for(String s:map.keySet()){
			pq.add(new Node(s,map.get(s)));
		}

		//print the top 5 strings
		int j=1;
		while(j<6){
			System.out.println(pq.peek().str+" "+pq.peek().freq);
			pq.poll();
			j++;
		}
		
	}
	
	public static class Node implements Comparable{
		String str;
		Integer freq;
		
		public Node(String str,Integer freq){
			this.str=str;
			this.freq=freq;
		}
		
		public int compareTo(Object o) {
			Node t=(Node)o;
			return t.freq-this.freq;
		}
			
			//to return the smallest first
			//return i1-i2;
	}

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String[] a={"abc", "bcd", "abc", "cde", "bcd", "abc","pqr","pqr","lkm","opt","lkm"};
		returnTopFive(a);
	}
}



