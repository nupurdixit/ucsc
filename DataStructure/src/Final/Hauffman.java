package Final;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

/*
 * File Name: Hauffman.java 
 * Hauffman encode and decode algorithms
 * 
 * @author Nupur Dixit
 * @year 2016
 */

public class Hauffman{
	static int IdCounter=50;
	String code="";
	String s;
	boolean show;
	String dotfilename;
	Tree root;
	
	
	public Hauffman(){
		s=null;
		show=false;
		dotfilename=null;
	}
	
	public Hauffman(String s, boolean show, String dotfilename){
		this.s=s;
		this.show=show;
		this.dotfilename=dotfilename;
	}
	
	/*
	 * Create a class Tree
	 */
	public static class Tree implements Comparable{
		Node root;
		
		public Tree(Tree t1,Tree t2){
			root=new Node();
			root.left=t1.root;
			root.right=t2.root;
			root.freq=t1.root.freq+t2.root.freq;
		}
		
		public Tree(int id,int freq,char letter,Node left,Node right){
			root=new Node(id,freq,letter,left,right);
		}
		
		public boolean hasOneNode(){
			if(root.isLeaf()){
				return true;
			}
			return false;
		}
	
		
		public int compareTo(Object o){
			Tree n;
			
			if(o instanceof Tree){
				n=(Tree)o;
				
			}
			else{
				return 0;
			}
			
			return (root.freq < n.root.freq) ? -1 : ((root.freq > n.root.freq) ? 1 : 0);
				
			
			
		}
	
	/*
	 * Create a class Node
	 */
		static class Node {	
			public int id;
			public int freq;
			public char letter;
			public Node left;
			public Node right;
			
			Node(){
				id=IdCounter++;
			}
			
			Node(int id,int freq,char letter,Node left,Node right) {
				this.id=IdCounter++;
				this.freq=freq;
				this.letter=letter;
				left = null;
				right = null;
				
			}
			
			//Check if the Node is the leaf
			public boolean isLeaf()
			{
				return (left == null && right==null);
				
			}
		}
	}
	
	/*
	 * method to Decode the string
	 */
	
	public String decode(){
		HashMap<Character,Integer> letterSet=new HashMap<Character,Integer>();
		letterSet=countOccurence(s); //Counts the occurence of each letter in the string and stores in HashMap
		root=buildHauffmanTree(letterSet); //Builds Tree 
		//generateHauffmanTreeDot(root.root,s,root); //generates DOT tree for Hauffman Tree
		//levelOrder(root.root);
		HashMap<Character,String> decode=getDecoding(root); //Decodes the string
			for(int i=0;i<s.length();i++){
				code+=decode.get(s.charAt(i));
			}
		
		return code; //returns the decoded string

	}
	
	/*
	 * Method to count the occurrence of each character in the given string
	 */
	public HashMap<Character,Integer> countOccurence(String s){
		HashMap<Character,Integer> letterSet=new HashMap<Character,Integer>();
		
		char[] charArray=s.toCharArray();
		
		for(char i:charArray){
			letterSet.put(i, letterSet.get(i)==null ? 1 : letterSet.get(i)+1);
		}

		return letterSet;
	}
	
	/*
	 * method to sort the values using priority queue
	 */

	public static Tree buildHauffmanTree(HashMap<Character,Integer> letterSet){
		PriorityQueue<Tree> pq=new PriorityQueue<Tree>();
		
		for(char key:letterSet.keySet()){

			pq.add(new Tree(IdCounter,letterSet.get(key),key,null,null));
		
		}
		
		while(pq.size()>1){
			Tree t1=pq.poll();
			Tree t2=pq.poll();
			pq.add(new Tree(t1,t2));
		}
		
		return pq.poll();
		
	}
	
	
	/*
	 * gets the string of 0's and 1's associated with each character
	 */
	private static HashMap<Character,String> getDecoding(Tree root){
		HashMap<Character,String> decoding=new HashMap<>();
		//if the tree has only one node then decode else traverse the entire tree
		if(root.hasOneNode()){
			decoding.put(root.root.letter, ""+root.root.freq);
		}
		else{
			traverse(root.root,"",decoding);
		}
		
		return decoding;
	}
	
	/*
	 * level order traversal
	 */
	public int[] levelOrder(Tree.Node root){
		//System.out.println("\nInside Level Order");
		int []a=new int[200];
		Queue<Tree.Node> q=new LinkedList<>();
		q.add(root);
		int k=0;
		while(!q.isEmpty()){
			Tree.Node n=q.remove();
			a[k++]=n.freq;
			System.out.print(n.freq);
			if(n.left!=null){
				q.add(n.left);
				System.out.print("->"+n.left.freq);
				
			}
			if(n.right!=null){
				q.add(n.right);
				System.out.print("->"+n.right.freq);
				System.out.println();
			}
			
		}
		System.out.println("Level Order exits\n");
		return a;
	}
		
		
	
	/*
	 * to assign code to each node
	 */
	
	private static void traverse(Tree.Node node,String code,HashMap<Character,String> decode){
		
		if(node.isLeaf()){
			decode.put(node.letter, code);
		}
		else{
			if(node.left!=null){
				traverse(node.left,code+"0",decode);
			}
			if(node.right!=null){
				traverse(node.right,code+"1",decode);
			
			}
		}
	}
		
	/*
	 * generating dot tree for Hauffman Tree
	 */
	private  void generateHauffmanTreeDot(Tree.Node node,String s,Tree root){
		
		System.out.println("\ndigraph Hauffman{");
		System.out.println("label =\"" +s+ "\" ");
		if(root.hasOneNode()){
			System.out.println("\""+node.id+"\\n"+node.freq+"\\n "+node.letter+"\"");
		}
		else{
			DotTraverse(node);
		}
		System.out.println("}\n");
	}
	
	/*
	 * populating the dot tree with required parameters
	 */
	private void DotTraverse(Tree.Node node){

			if(node.left!=null){
				if(!node.left.isLeaf()){
					System.out.print("\""+node.id+"\\n"+node.freq+"\"");
					System.out.println("->"+"\""+node.left.id+"\\n"+node.left.freq+"\""+" [color=red]");
					DotTraverse(node.left);
				}
				else{
					System.out.print("\""+node.id+"\\n"+node.freq+"\"");
					System.out.println("->"+"\""+node.left.id+"\\n"+node.left.freq+"\\n "+node.left.letter+"\""+" [color=red]");
					DotTraverse(node.left);
				}
								
			}
			if(node.right!=null){
				if(!node.right.isLeaf()){
					System.out.print("\""+node.id+"\\n"+node.freq+"\"");
					System.out.println("->"+"\""+node.right.id+"\\n"+node.right.freq+"\""+" [color=blue]");
					DotTraverse(node.right);
				}
				else{
					System.out.print("\""+node.id+"\\n"+node.freq+"\"");
					System.out.println("->"+"\""+node.right.id+"\\n"+node.right.freq+"\\n "+node.right.letter+"\""+" [color=blue]");
					DotTraverse(node.right);
				}
			}
	}
	
	
	/*
	 * Method to encode the string
	 */
		
	public String encode(){
		String encode="";
		encode=TraverseToEncode(root.root,code);
		return encode;
	}
		
	/*
	 * traverse the tree in order to encode back the original string
	 */
	public String TraverseToEncode(Tree.Node node,String code){
		node=root.root;
		StringBuilder sb=new StringBuilder();
		if(code.length()==1)
			sb.append(node.letter);
		else{
			for(int i=0;i<code.length();){
				if(node.isLeaf()){
					sb.append(node.letter);
					if(code.length()>1)
						node=root.root;
					continue;
				}
				if(String.valueOf(code.charAt(i)).equals("1")){
					node=node.right;
					i++;
				}
				else{
					node=node.left;
					i++;
				}
				if(i>=code.length())
					if(node.isLeaf()){
						sb.append(node.letter);
						node=root.root;
						continue;
					}
				}
		}
		
		return sb.toString();
	}		
}


	
	
