package Final;

import java.util.HashMap;

public class CalculateFrequency extends Hauffman {
	HashMap<Character,Integer> letterSet;
	String s;
	
	CalculateFrequency(String s){
		this.s=s;
		this.letterSet=letterSet;
	}
	
	public HashMap<Character,Integer> countOccurence(String s,HashMap<Character,Integer> letterSet){
		System.out.println("s is"+ s);
		char[] charArray=s.toCharArray();
		System.out.println("The given string is"+ s);
		for(char i:charArray){
			letterSet.put(i, letterSet.get(i)==null ? 1 : letterSet.get(i)+1);
		}
		
		for(Character key:letterSet.keySet()){
			System.out.println("occurence of key '"+ key +"' is "+letterSet.get(key));
		}
		return letterSet;
	}
	
	public buildTree
}

class NodeWithFrequency implements Comparable<NodeWithFrequency>{
	
	public Node node;
	//public HashMap<Character,Integer> letterSet;
	public Character key;
	public Integer value;
	
	public NodeWithFrequency(Node node,Character key,Integer value){
		this.node=node;
		this.key=key;
		this.value=value;
		
	}
	
	@Override
	public int compareTo(NodeWithFrequency other) {
		// TODO Auto-generated method stub
		return this.value.compareTo(other.value);
		//return 0;
	}
	
}
