package HomeworkDay5;

import java.util.LinkedList;


public class IntSlist extends node{
	private node first ;
	private node last;
	private int num ;
	private static final IntUtil u = new IntUtil();
	private static final int INTSLISTNULL = -99999999 ;
	
	public IntSlist() {
		first = null ;
		last = null ;
		num = 0;
	}
				
				
	public void add(int x) {
		node n = new node(x) ;
		if (first != null) {
		last.next = n ;
		}else {
		first = n ;
		}
		last = n ;
	//	incSize();
	}
	
	private void find(int x, node [] nodes) {
		nodes[0] = first ;
		nodes[1] = null ;
		while (nodes[0] != null) {
		if (nodes[0].d == x) {
		return ;
		}
		nodes[1] = nodes[0] ;
		nodes[0] = nodes[0].next ;
		}
	}
	
	public boolean contains(int x) {
		node [] nodes = new node[2] ;
		find(x,nodes) ;
		return (nodes[0] != null) ? true : false ;
	}
	
	public boolean remove(int x) {
		node [] nodes = new node[2] ;
		find(x,nodes) ;
		if (nodes[0] != null) {
		if ((nodes[0] == first) && (nodes[0] == last)) {
		//Does list has only one element
		first = null ;
		last = null ;
		}else if (nodes[0] == first) {
		//first element being removed and list
		//has more than 1 element
		first = nodes[0].next ;
		}else if (nodes[0] == last) {
		//last element being removed and list
		//has more than 1 element
		nodes[1].next = null ;
		last = nodes[1] ;
		}else {
		//You are removing middle element
		nodes[1].next = nodes[0].next ;
		}
		nodes[0] = null ; //Garbage collection
		//decSize();
		return true ;
		}else {
		return false ;
		}
	}
	
	public void pLn() {
		node n = first ;
		while (n != null) {
		System.out.print(n.d);
		if (n.next == null) {
		System.out.print("->NIL");
		}else {
		System.out.print("->");
		}
		n = n.next ;
		}
		System.out.println("");
	}
	
	public int get(int pos) {
		node t = first ;
		int n = 0 ;
		while (t != null) {
		if (pos == n) {
		return t.d ;
		}
		t = t.next ;
		n++ ;
		}
		return INTSLISTNULL;
	}
		
		
		
	/*public void assertGR(LinkedList<Integer> g){
		int gs = g.size() ;
		if (gs != size()) {
		u.myassert(false) ;
		}
		int i = 0 ;
		node n = first ;
		while (n != null) {
		u.myassert(n.d == (int)g.get(i));
		++i ;
		n = n.next ;
		}
	

	}*/
	
	public static IntSlist buildSlist(int [] a, boolean ascend) {
		IntSlist l = new IntSlist();
		int n=a.length;
		 for (int i = 0; i < n; ++i) {
		      int x = a[i] ;
		      l.add(x);
		  }
		
		return l ;
	}
	
	public static void main(String[] args){
		//IntSlist s=new IntSlist();
		int[] a={2,4,3,1,6,7};
		IntSlist s=buildSlist(a,true);
		s.pLn();
		node n1=s.Nth(4);
		System.out.println("4th element is: "+n1.d);
	}
	
	public node Nth(int pos){
		if(pos==1){
			return this;
		}
		else if(pos<1){
			return null;
		}
		else{
			first=first.next;
			return first.Nth(pos-1);
		}
	}
		  
}

class node{
	int d;
	node next;
	
	public node(){
		d=0;
		next=null;
		
	}
	
	public node(int x,node next){
		this.d=x;
		this.next=next;
	}
	
	public node(int x){
		this(0,null);
	}
	
	
}
