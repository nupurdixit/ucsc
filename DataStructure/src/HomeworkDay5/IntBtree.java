package HomeworkDay5;

import java.util.LinkedList;
import java.util.Queue;

//import Final.IntUtil;

public class IntBtree {
	protected node root;
	private int num;
	private int height ;
	//Largest depth of any node is the height of the tree
	protected static final IntUtil u = new IntUtil();
	public static final int NULL = -99999999;
	
	public IntBtree(){
		root=null;
		num=0;
		height=0;
	}
	
	public IntBtree(int[][] t) {
		this();
		buildTreeFromUserSpec(t);
		levelOrder();
	}
	
	public int height(){
		return this.height;
	}
	
	public int size(){
		return num;
	}
	
	public boolean isLeaf(node n) {
		return (n.left == null && n.right == null) ? true : false;
	}
	
	protected node isOneKid(node n) {
		if (n.left == null && n.right != null) {
		return n.right;
		}
		if (n.left != null && n.right == null) {
		return n.left;
		}
		return null ;
	}
	
	
	private void depth_r(node r) {
		if (r != null) {
			if (r == root) {
			r.depth = 0;
			} 
			else {
				r.depth = r.father.depth + 1;
				if (r.depth > height) {
					height = r.depth ;
				}
			}
			depth_r(r.left);
			depth_r(r.right);
		}
	}
	
	public void computeDepth() {
		depth_r(root);
	}
	
	public node buildNode(int x){
		node n=new node(x);
		return n;
	}
	
	public void buildTree(){
		
	}
	public void buildTreeFromUserSpec(int[][] t) {
		// Step1: Find max
		int size = t.length;
		if (size > 0) {
			int max = 0;
			int min = 9999;
			for (int i = 0; i < size; ++i) {
				int[] a = t[i];
				u.myassert(a.length == 3);
				for (int j = 0; j < a.length; ++j) {
					if (a[j] > max) {
						max = a[j];
					}
					if((a[j]!=NULL) && (a[j]<min)){
						min=a[j];
					}
				}
				u.myassert(min>=0);
			}
			node[] tn = new node[max + 1];
			for (int i = 0; i < size; ++i) {
				int[] a = t[i];
				// Father
				if (tn[a[0]] == null) {
					tn[a[0]] = buildNode(a[0]);
					}
				else if ((tn[a[0]].left != null) || (tn[a[0]].right != null)) {
					System.out.println(a[0]
					+ " has already keft or right kid. Bug in input");
					root = null;
					num = 0;
					return; // Tree not constructed
				}
			// Left kid
				if (a[1] != NULL) {
					if (tn[a[1]] == null) {
						tn[a[1]] = buildNode(a[1]);
					}
					tn[a[0]].left = tn[a[1]];
					tn[a[1]].father = tn[a[0]];
					}
				// Right Kid
					if (a[2] != NULL) {
						if (tn[a[2]] == null) {
							tn[a[2]] = buildNode(a[2]);
						}
						tn[a[0]].right = tn[a[2]];
						tn[a[2]].father = tn[a[0]];
					}
			}
			
			root=null;
			for (int i = 0; i < tn.length; ++i) {
				if ((tn[i] != null) && (tn[i].father == null)) {
					if (root != null) {
						System.out.println("Two roots " + tn[i].d + " Bug in input");
						root = null;
						num = 0;
						return; // Tree not constructed
					}
					root = tn[i];
				}
			}
		}
	}
	
	public int[] levelOrder(){
		int []a=new int[10];
		Queue<node> q=new LinkedList();
		q.add(root);
		int k=0;
		while(!q.isEmpty()){
			node n=q.remove();
			a[k++]=n.d;
			System.out.print(n.d);
			if(n.left!=null){
				q.add(n.left);
				System.out.print("->"+n.left.d);
			}
			if(n.right!=null){
				q.add(n.right);
				System.out.print("->"+n.right.d);
				System.out.println();
			}
			
		}
		
		return a;
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [][]t = {
					{6,2,7} ,
					{2,1,4},
					{7,IntBtree.NULL,9},
					{4,3,5},
					{9,8,IntBtree.NULL}
					};
		IntBtree tree=new IntBtree(t);
		
		
	}

}

class node {
	 int d;
	 int depth;
	 node father;
	 node left;
	 node right;
	
	public node(int x) {
	d = x;
	depth = 0;
	father = null;
	left = null;
	right = null;
	}
}
