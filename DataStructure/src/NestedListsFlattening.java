import java.util.ArrayList;
import java.util.List;
/**
 * Program to convert lists of lists in a single list
 * @author NUPUR
 *
 */
public class NestedListsFlattening {
	
	static String delimeter="]|\\[";
	public static void listFlatten(String a){
		String[] splits=a.split(delimeter);
		StringBuilder sb=new StringBuilder();
		sb.append("[");
		
		for(String str:splits){
			
			sb.append(str);
			
		}
		sb.append("]");
		System.out.println(sb.toString());
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String a="[a,[b,c],[e,[f,g]],h]";
		listFlatten(a);
	}
	
	

}
