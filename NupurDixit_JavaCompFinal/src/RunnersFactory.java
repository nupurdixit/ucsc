import java.util.Scanner;

/**
 * This is a factory class which checks the choice entered by the user and reads the data  from respective input source.
 * 
 * @author NUPUR
 *
 */
public class RunnersFactory {
	
		/**
		 * This method lets the input source be read based on the choice entered by the user
		 * 1.Derby Database
		 * 2.XML File
		 * 3.Text File
		 * 4.Two default runners
		 * 5.Exit
		 * 
		 * @param choice - This is entered by the user in the main application
		 * @return - IRunnerReader reference
		 */
	    public static IRunnerReader getRunnerInputSource(int choice)
	    {
	    	Scanner sc=new Scanner(System.in);
	        IRunnerReader runnerReader=null; 
	    		switch (choice) {
		    		case 1:
		    	        runnerReader = new RunnersDBFile();
		    			break;
		    		case 2:
		    			String XMLFileName=Validator.FileExtensionValidation(sc, "Enter XMLFile name(with extension): ",choice);
		    			runnerReader = new RunnersXMLFile(XMLFileName);
		    			break;
		    		case 3:
		    			String textFileName=Validator.FileExtensionValidation(sc, "Enter TextFile name(with extension): ",choice);
		    			runnerReader = new RunnersTextFile(textFileName);	
		    			break;
		    		case 4:
		    			runnerReader=new DefaultTwoRunners();
		    			break;
		    		case 5:
		    			System.out.println("Thank you for using my Marathon Race Program");
		    			System.exit(0);
		    		default:
		    	        break;
	    		}
	        return runnerReader;
	    }
	}

