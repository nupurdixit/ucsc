import java.util.Random;
/**
 * Class which enables each runner to run as a thread
 * 
 * @author NUPUR
 *
 */
public class ThreadRunner extends Thread{
	private String runnerName;
	private int restPercentage;
	private int runnersSpeed;
	private Random random = new Random();
	private static volatile boolean running=true;
	private static Object obj=new Object();
	
	/**
	 * Class Constructor
	 */
	public ThreadRunner(){
		this("",0,0);
	}
	
	/**
	 * 
	 * @param runnerName - Name of the runner
	 * @param runnersSpeed - speed of the runner - that is, how many meters the runner travels in each move.
	 * @param restPercentage - an integer value from 1 to 100 indicating the likelihood that on any given move the runner
	 *                         will rest instead of running
	 * 
	 */
	public ThreadRunner(String runnerName,int runnersSpeed,int restPercentage){
		this.runnerName=runnerName;
		this.restPercentage=restPercentage;
		this.runnersSpeed=runnersSpeed;
		running = true;
	}
	
	/**
	 *  
	 * @return- runnerName which is the name of the runner  
	 */
	public String getRunnerName(){
		return runnerName;
	}
	
	/**
	 * Sets the runnerName
	 * 
	 * @param runnerName -  Name of the runner
	 */
	public void setRunnerName(String runnerName){
		this.runnerName=runnerName;
	}
	
	/**
	 * 
	 * @return - restPercentage - an integer value from 1 to 100 indicating the likelihood that on any given move the runner
	 *                         will rest instead of running
	 */
	public int getRestPercentage(){
		return restPercentage;
	}
	
	/**
	 * Sets the restPercentage
	 * 
	 * @param restPercentage - an integer value from 1 to 100 indicating the likelihood that on any given move the runner
	 *                         will rest instead of running
	 */
	public void setRestPercentage(int restPercentage){
		this.restPercentage=restPercentage;
	}
	
	
	/**
	 * 
	 * @return- runnersSpeed -  speed of the runner - that is, how many meters the runner travels in each move.
	 */
	public int getRunnersSpeed(){
		return runnersSpeed;
	}
	
	
	/**
	 * Sets the runnersSpeed
	 * 
	 * @param runnersSpeed - speed of the runner - that is, how many meters the runner travels in each move.
	 */
	public void setRunnersSpeed(int runnersSpeed){
		this.runnersSpeed=runnersSpeed;
	}
	
	/**
	 * This method would be invoked by thread which completes the 1000m race
	 */
	public synchronized void finished(){
		running=false;
		System.out.println(this.getRunnerName()+" : I finished!");
		System.out.println("\nThe race is over! The "+this.getRunnerName()+" is the winner\n");
	}
	
	/**
	 * This method decides the winner of the race.The thread which completes the 1000m race calls the finished method and 
	 * thereafter rest of the threads do not run.This is done by checking a volatile boolean flag.
	 */
	@Override
	public void run() {
			for(int i=this.getRunnersSpeed();i<=1000;){
				synchronized(obj){
					if(!running){
						System.out.println(this.getRunnerName()+" :You beat me fair and square");
						break;
					}
				}
				//Check the rest percentage by generating a random number between 1 and 100.if less, than sleep else run
				if(random.nextInt(100) <= this.getRestPercentage()){
					try{
						Thread.sleep(100);
					}
					catch(InterruptedException e){
						e.printStackTrace();
					}
				
				}
				else{
					
					System.out.println(this.getRunnerName()+ ": "+i);
					i+=this.getRunnersSpeed();
					
					//if the distance covered >=1000 then call finished () , wherein that thread is declared as winner
					synchronized(obj){
						if(i >= 1000){
							System.out.println(this.getRunnerName()+ ": "+i);
							finished();
							break;
						}
					}
					
				}
			}
	}

	@Override
	public String toString() {
		return "ThreadRunner [threadValue=" + runnerName + ", restPercentage=" + restPercentage + ", runnersSpeed="
				+ runnersSpeed + "]";
	}
	
	
	
}
