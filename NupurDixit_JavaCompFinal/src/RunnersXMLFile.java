import java.io.FileReader;
import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import javax.xml.stream.*;

/**
 * This class lets the runners information be read from the text file and writes it into an array list.
 * 
 * @author NUPUR
 *
 */
public class RunnersXMLFile implements IRunnerReader{
	private Path runnersPath=null;
	private ArrayList<ThreadRunner> runnersList=null;
	
	/**
	 * Parameterized Constructor
	 * 
	 * @param filename - This is the filename entered by the user from which the runners data has to be read.
	 */
	public RunnersXMLFile(String filename){
		runnersPath=Paths.get(filename);
	}
	
	
	/**
	 * This method read the runners data from the XML file and writes it into an array list.
	 * 
	 * @return - Array list of runners details read from the text file.
	 */
	public ArrayList<ThreadRunner> getRunnersInfo(){
		
		 // if the XML file has already been read then do not read it again
	
		if(runnersList!=null)
			return runnersList;
		
		runnersList=new ArrayList<>();
		ThreadRunner threadRunnerObj=null;
		
		//If the filename entered by the user exists in the specified path, then process it else throw a message saying 
		//no such file exists
		if(Files.exists(runnersPath)){
			
			 // create the XMLInputFactory Object
			 
			XMLInputFactory inputFactory=XMLInputFactory.newFactory();
			try{
				
				 // create a XMLStreamReader object
				 
				FileReader fileReader=new FileReader(runnersPath.toFile());
				XMLStreamReader reader=inputFactory.createXMLStreamReader(fileReader);
				
				// read the runners details from the file
				 
				while(reader.hasNext()){
					int eventType=reader.getEventType();
					switch(eventType)
					{
						case XMLStreamConstants.START_ELEMENT:
							String elementName=reader.getLocalName();
							if(elementName.equals("Runner")){
								threadRunnerObj=new ThreadRunner();
								String runnerName=reader.getAttributeValue(0);
								threadRunnerObj.setRunnerName(runnerName);
							}
							
							if(elementName.equals("RunnersMoveIncrement")){
								
								int runnersSpeed=Integer.parseInt(reader.getElementText());
								threadRunnerObj.setRunnersSpeed(runnersSpeed);
							}
							
							if(elementName.equals("RestPercentage")){
								
								int restPercentage=Integer.parseInt(reader.getElementText());
								threadRunnerObj.setRestPercentage(restPercentage);
							}
							break;
						case XMLStreamConstants.END_ELEMENT:
							elementName=reader.getLocalName();
							if(elementName.equals("Runner")){
								runnersList.add(threadRunnerObj);
							}
							break;
						default:
							break;
					}
					reader.next();
					
				
				}
			}
			//in case the data in the xml file is corrupted then return null, the user would be displayed new menu thereafter
			//in the main user application
			catch(IOException | XMLStreamException e ){
				return null;
			}
		
		}
		else{
			System.out.println("No such file exists!");
		
		}
		return runnersList;
	}	
}
