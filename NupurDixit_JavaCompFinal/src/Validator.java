import java.util.Scanner;
/**
 * This is a class to Validate the choices entered by a user
 * 
 * @author NUPUR
 *
 */
public class Validator {
	/**
	 * Method to validate the choice entered by the user for choosing the input source from the Menu
	 * 
	 * @param sc - Scanner reference
	 * @param prompt - message to user e.g, Enter the choice
	 * @return - choice entered by the user
	 */
	public static int getInt(Scanner sc, String prompt){
		int choiceInt=0;
		
		do{
			System.out.print(prompt);
			String choice=sc.nextLine();
			
			//Exception Handling during String to integer conversion.This is to handle NumberFormatException	
			try{
				//Convert the choice given as String to Integer
				choiceInt=Integer.parseInt(choice); 
				
				//logic to check if the choice entered is not in range
				if(choiceInt <1 || choiceInt >5 || choice.length()>1){
					
					//Display the message if the user enters any option other than 1 to 5.
					System.out.println("You have not entered a number between 1 and 5.Try again.\n");
				}
				else{
					break;
				}
			//Catch the NumberFormatException which would arise if the String cannot be parsed to Integer
			}catch (final NumberFormatException e){
					//Display the message if an invalid choice has been entered by the user
					System.out.println("You have entered an invalid choice,please re-enter your choice.\n ");
			}
			
			
		}while(true);
		
		return choiceInt;

	}
	

	/**
	 * Method to check whether the File name has been entered without an extension or extension other than "xml" in case of 
	 * XML file and "txt" in case of text file.
	 * If yes, then the user is given a choice to re-enter the name
	 * 
	 * @param sc - Scanner reference
	 * @param prompt - Message for user e.g.ENter the file name
	 * @param choice - choice in the menu
	 * @return filename - name of the file from where the data needs to be read
	 */

	 
	 public static String FileExtensionValidation(Scanner sc, String prompt,int choice){
		 String fileName;
		 String[] fileNameWithExtension;
		 
			while(true){
				System.out.print(prompt);
				fileName=sc.nextLine();
				fileNameWithExtension=fileName.split("\\.");
				String extension;
				if(choice==2){
					extension="xml";
				}
				else{
					extension="txt";
				}		
				if(fileNameWithExtension.length==1 || !fileNameWithExtension[1].equals(extension)){
					System.out.println("Invalid File extension.Please try again\n");
				}
	 			else{
	 				break;
	 			}		
			}
			return fileName;
	 
	 }
}
