import java.util.*;

/**
 * This is the class where the user enters the choice for getting the data from any of the 4 input source
 * and watch the winner of the Marathon race.
 * 
 * @author NUPUR
 *
 */
public class UserApp {
	
	/**
	 * Main method which displays the menu to the user for selecting a choice of the input source.It then starts the threads equal
	 * to the runners read from the input source.
	 * The menu is displayed again till the user decides to exit by giving option 5.
	 * 
	 * @param args - String arguments
	 * @throws InterruptedException - Exception Handling
	 */
	public static void main(String[] args) throws InterruptedException {
		
		Scanner sc=new Scanner(System.in);
		IRunnerReader runnerReader=null;
		int choice=0;
		
		while(true){ 
			displayMenu();
			choice=Validator.getInt(sc,"Enter your choice: ");
			
			runnerReader=RunnersFactory.getRunnerInputSource(choice);
			
			ArrayList<ThreadRunner> runners = runnerReader.getRunnersInfo();
			
			//This is handled in case any file(xml or text) entered by the user has a corrupted data
			if(runners==null){
				System.out.println("Data returned by the input source is corrupted\n");
				System.out.println("Press any key to continue");
				sc.nextLine();
				continue;
			}
			
			//Create threads equal to the number of runners in the array list
			for(int i=0;i<runners.size();i++){
				runners.get(i).start();
				
			}
			
			if(!runners.isEmpty()){
				System.out.println("\nGet set...Go!");
			}
			
			
			for(int i=0;i<runners.size();i++){
				runners.get(i).join();
			}
		
			System.out.println("\n\nPress any key to continue");
			sc.nextLine();
		}
	}
	
	/**
	 * This method displays the menu
	 */
	public static void displayMenu(){
		System.out.println("Welcome to the Marathon Race Runner Program");
		System.out.println("\nSelect your data source:");
		System.out.println("\n1.  Derby database\n2.  XML file\n3.  Text file\n4.  Default two runners\n5.  Exit\n");	
	}

}
