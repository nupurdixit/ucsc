import java.util.ArrayList;

/**
 * Interface which would be implemented by all the input source readers viz.RunnersDBFile,RunnersTextFile,RunnersXMLFile and 
 * DefaultTwoRunners
 */
public interface IRunnerReader{
	ArrayList<ThreadRunner> getRunnersInfo();
	
}
