import java.util.ArrayList;
/**
 * This class contains the information for two runners whose values are hard-coded.
 * 
 * @author NUPUR
 *
 */
public class DefaultTwoRunners implements IRunnerReader {
	ArrayList<ThreadRunner> runnersList;
	
	/**
	 * This method read the hardcoded runners information and add it to the array list.
	 * 
	 * @return -  Array list of runners 
	 */
	public ArrayList<ThreadRunner> getRunnersInfo(){
		/**
		 * 1st  Runner Information
		 */
		String runner1="Tortoise";
		int runner1Speed=10;
		int runner1RestPercentage=30;
		
		/**
		 * 2nd Runner Information
		 */
		String runner2="Hare";
		int runner2Speed=100;
		int runner2RestPercentage=80;
		
		//array list to contain the runners information
		runnersList=new ArrayList<>();
		ThreadRunner threadRunner1=new ThreadRunner(runner1,runner1Speed,runner1RestPercentage);
		runnersList.add(threadRunner1);
		ThreadRunner threadRunner2=new ThreadRunner(runner2,runner2Speed,runner2RestPercentage);
		runnersList.add(threadRunner2);
		
		return runnersList;
	}
	
}
