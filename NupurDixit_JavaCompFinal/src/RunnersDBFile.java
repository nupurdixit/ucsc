import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * This program would let the runners data get fetched from Database. The data in this case is stored in RunnersStats table
 * inside RunnersDB database.
 * 
 * @author NUPUR
 * 
 */
 
public class RunnersDBFile implements IRunnerReader {
	ArrayList<ThreadRunner> runnersList;
	
	/**
	 * This method helps setting up the database connection
	 * 
	 * @return - Connection reference
	 */
	private Connection connect(){
		Connection connection=null;
		try{
			String dbDirectory="Resources";
			System.setProperty("derby.system.home",dbDirectory);
			String url="jdbc:derby:RunnersDB";
			String user="";
			String password="";
			connection=DriverManager.getConnection(url,user,password);
		}
		catch(SQLException sqle){
			System.err.println("Error loading database driver: "+sqle);
		}
		return connection;
	}
	
	/**
	 * This method finds the runners information by running a select query on database and then add it to the array list.
	 * 
	 * @return - Array list of runners
	 */
	public ArrayList<ThreadRunner> getRunnersInfo(){
		
		try{
			runnersList=new ArrayList<>();
			Connection connection=connect();
			
			//Run a query on RunnersStats table in order to get the information of all the runners
			String query="SELECT Name ,RunnersSpeed ,RestPercentage "+
			"FROM RunnersStats ";
			
			PreparedStatement ps=connection.prepareStatement(query);
			ResultSet rs=ps.executeQuery();
			
			while(rs.next()){
				String threadValue=rs.getString("Name");
				int runnersSpeed=rs.getInt("RunnersSpeed");
				int restPercentage=rs.getInt("RestPercentage");
				ThreadRunner threadRunner=new ThreadRunner(threadValue,runnersSpeed,restPercentage);
				runnersList.add(threadRunner);
				
			}
			rs.close();
			ps.close();
			connection.close();
			return runnersList;
			
		}
		catch(SQLException sqle){
			sqle.printStackTrace();
 			return null;
		}
	}
	
}
