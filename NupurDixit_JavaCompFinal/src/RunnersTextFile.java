  import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;

/**
 * This class lets the runners information be read from the text file and written into an array list.
 * 
 * @author NUPUR
 *
 */
public class RunnersTextFile implements IRunnerReader {
	private ArrayList<ThreadRunner> runnersList;
	private Path runnersPath=null;
 	private File runnersFile=null;
	private final String STRING_SEP="\\t+"; //strings might be separated by one or more tabs thus using the pattern as the delimiter
	
	/**
	 * Parameterized Constructor
	 * 
	 * @param filename - This is the filename entered by the user from which the runners data has to be read.
	 */
	public RunnersTextFile(String filename){
		runnersPath=Paths.get(filename);
		runnersFile=runnersPath.toFile();
	}
	
	/**
	 * This method read the runners data from the text file and writes it into an array list.
	 * 
	 * @return - Array list of runners details read from the text file.
	 */
	public ArrayList<ThreadRunner> getRunnersInfo(){
		BufferedReader in=null;
		
		// if the file has already been read then do not read it again
		
		if(runnersList!=null){
			return runnersList;
		}
		
		runnersList=new ArrayList<>();
		
		//If the filename entered by the user exists in the specified path, then process it else throw a message saying 
		//no such file exists
		
		if(Files.exists(runnersPath)){
			try{
				in=new BufferedReader(new FileReader(runnersFile));
				String line=in.readLine();
				
				while(line!=null){
					String[] runnersInfo=line.split(STRING_SEP);
					String threadValue=runnersInfo[0].trim();
					int runnersSpeed=Integer.parseInt(runnersInfo[1].trim());
					int restPercentage=Integer.parseInt(runnersInfo[2].trim());
					ThreadRunner threadRunner=new ThreadRunner(threadValue,runnersSpeed,restPercentage);
					runnersList.add(threadRunner);
					line=in.readLine();
				}
			}
			//in case the data in the text file is corrupted then return null, the user would be displayed new menu thereafter
			//in the main user application
			catch(Exception e){
				return null;
			}
			finally{
				close(in);
			}
		}
		else{
			System.out.println("No such Text File exists!");
		}
		
		return runnersList;
	}
	
	
	/**
	 * private method that closes the I/O stream
	 * 
	 * @param stream 
	 */
	private void close(Closeable stream){
		try{
			if(stream!=null){
				stream.close();
			}
		}
		catch(IOException ioe){
			ioe.printStackTrace();
		}
	}
	
}
