package Day6;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class CountriesTextFile {
	private ArrayList<String> countries;
	private Path countriesPath=null;
	private File countriesFile=null;
	
	public CountriesTextFile() throws IOException{
		countriesPath=Paths.get("countries.txt");
		countriesFile=countriesPath.toFile();
		//countries=new ArrayList<>();
		this.checkFile();
		//countries=new ArrayList<>();
	}
	
	public ArrayList<String> getCountries() throws IOException {
		/*
		 * if the file has been already read then return the list of countries
		 */
//		if(countries!=null)
//			return countries;
//		
		BufferedReader in=null;
		try{
			in=new BufferedReader(new FileReader(countriesFile));
			countries=new ArrayList<>();
			String line=in.readLine().trim();
			while(line!=null){
				//System.out.println(line);
				countries.add(line);
				line = in.readLine();     
			}
		}
		catch(IOException e){
			System.out.println(e);
			return null;
		}
		finally{
			close(in);
		}
		return countries;
	}
	
	/*
	 * A private method that closes the I/O stream
	 */
	
	
		
	public void addCountry(String countryName) throws IOException{
		this.getCountries();
		countries.add(countryName);
		this.saveCountries(countries);
	}
	
	public boolean saveCountries(ArrayList<String> countries){
		PrintWriter out=null;
		try {
			checkFile();
			out = new PrintWriter(
							   new BufferedWriter(
                		       new FileWriter(countriesFile)));
			for(String countryList:countries){
				out.println(countryList);
			}
			
		}
		 catch(IOException e)
        {
            System.out.println(e);
            return false;
        }
		finally{
			out.close();
		}

        return true;
	}
		
	
	
	private void checkFile(){
		try{
			if(!countriesFile.exists())
				countriesFile.createNewFile();
		}
		catch(IOException ioe){
			ioe.printStackTrace();
		}
	}
	
	private void close(Closeable stream){
		try{
			if(stream!=null)
				stream.close();
		}
		catch(IOException ioe){
			ioe.printStackTrace();
		}
	}
	
}
