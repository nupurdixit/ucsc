package Day6;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import Day3.Validator;

public class CountriesApp {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		System.out.println("Welcome to the Countries Maintenance Application");
		System.out.println("\n1 - List Countries\n2 - Add a couuntry\n3 - Exit\n");
		System.out.println();
		//int menuNumber=Validator.getInt(sc, "Enter menu number");
		CountriesTextFile countriesDAO=new CountriesTextFile();
		//int menuNumber=Validator.getInt(sc, "Enter menu number");
		int menuNumber=0;
		do{
			menuNumber=Validator.getInt(sc, "Enter menu number",0,4);
			switch(menuNumber){
				case 1:
					ArrayList<String> countries=countriesDAO.getCountries();
					for(String countryList:countries)
					{
						System.out.println(countryList);
					}
					break;
				case 2:
					String countryToBeAdded=Validator.getString(sc, "Enter Country");
					countriesDAO.addCountry(countryToBeAdded);
					System.out.println("This country has been saved");
					break;
				case 3:
					System.out.println("Good Bye");
					System.exit(0);
					break;
				default:
					System.out.println("Wrong choice entered");
					break;
			}
		}while(menuNumber!=3);
		
				
		}

}

