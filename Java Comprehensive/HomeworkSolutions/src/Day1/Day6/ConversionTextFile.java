package Day6;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class ConversionTextFile {
	private ArrayList<Conversion> typesList;
	private Path conversionPath=null;
	private File conversionFile=null;
	
	
	public ConversionTextFile() throws IOException{
		String fileName="conversion_types.txt";
		String dirString="C:\\work\\UCSC\\Java Comprehensive\\HomeworkSolutions";
		conversionPath=Paths.get(dirString,fileName);
		if(Files.notExists(conversionPath)){
			Files.createFile(conversionPath);
		}
		conversionFile=conversionPath.toFile();
		typesList=this.getConversions();
		
	}
	public ArrayList<Conversion> getConversions(){

		if(typesList!=null)
			return typesList;
		
		typesList=new ArrayList<>();
		
		if(Files.exists(conversionPath)){
			try(BufferedReader in=new BufferedReader(
									new FileReader(conversionFile)))
			{
				String line=in.readLine().trim();
				while(line!=null){
					System.out.println(line);
					//typesList.add(line);
					line = in.readLine();     
				}
			}
			catch(IOException e){
				System.out.println(e);
				return null;
			}
		}
		
		
		return typesList;
	}
		
	
}
