package Day6;

import java.util.ArrayList;
import java.io.*;

public class CountriesBinaryFile
{
    private static File countriesFile = new File("countries.dat");

    public CountriesBinaryFile()
    {
        checkFile();
    }

    public static void main(String args[])
    {
        // you only need to run this method once
        initializeCountriesFile();
    }

    // use this method to create and initialize the binary file
    private static void initializeCountriesFile()
    {
        try
        {
            // create the countries file
            countriesFile.createNewFile();
        }
        catch(IOException ioe)
        {
            ioe.printStackTrace();
        }

        // create the initial countries list
        ArrayList<String> countries = new ArrayList<>();
        countries.add("India");
        countries.add("Japan");
        countries.add("Mexico");
        countries.add("Spain");
        countries.add("United States");

        // write the countries list to the binary file
        DataOutputStream out = null;
        try
        {
            // open output stream for overwriting
            out = new DataOutputStream(
                new BufferedOutputStream(
                new FileOutputStream(countriesFile)));

            for (String country : countries)
            {
                out.writeUTF(country);
            }
        }
        catch(IOException ioe)
        {
            ioe.printStackTrace();
        }
        finally
        {
            close(out);
        }

        System.out.println("The countries.dat file has been initialized.\n");
    }

    public ArrayList<String> getCountries()
    {
        // declare the input stream
        DataInputStream in = null;
        try
        {
            // check to make sure the file exists
            checkFile();

            // create the input stream
            in = new DataInputStream(
                new BufferedInputStream(
                new FileInputStream(countriesFile)));

            // create the array list
            ArrayList<String> countries = new ArrayList<>();

            while(in.available() > 0)
            {
                // read each string from the file
                String country = in.readUTF();

                // add each country to the array list
                countries.add(country);
            }

            return countries;
        }
        catch(IOException ioe)
        {
            ioe.printStackTrace();
            return null;
        }
        finally
        {
            close(in);
        }
    }

    public boolean saveCountries(ArrayList<String> countries)
    {
        DataOutputStream out = null;
        try
        {
            checkFile();

            // open output stream for overwriting
            out = new DataOutputStream(
                new BufferedOutputStream(
                new FileOutputStream(countriesFile)));

            // write each country to the file
            for (String country : countries)
            {
                out.writeUTF(country);
            }
        }
        catch(IOException ioe)
        {
            ioe.printStackTrace();
            return false;
        }
        finally
        {
            close(out);
        }
        return true;
    }

    // a private method that creates a blank file if the file doesn't already exist
    private static void checkFile()
    {
        try
        {
            if (!countriesFile.exists())
                countriesFile.createNewFile();
        }
        catch(IOException ioe)
        {
            ioe.printStackTrace();
        }
    }

    // a private method that closes the I/O stream
    private static void close(Closeable stream)
    {
        try
        {
            if (stream != null)
                stream.close();
        }
        catch(IOException ioe)
        {
            ioe.printStackTrace();
        }
    }

}
